
Gene Ontology analysis of gene expression changes in pollen induced by chronic heat stress
========================================================

Introduction
------------

This R markdown document describes using Bioconductor package called goseq to determine GO categories that are over-represented among differentially expressed genes.

Question we aim to answer include:

* What types of genes are unusually abundant among genes that are up- or down-regulated in tomato pollen undergoing chronic heat stress treatment?

To answer this question, we'll do the following analyses:

* Look for enriched categories among up-regulated genes
* Look for enriched categories among down-regulated genes
* Look for enriched categories among DE genes (both up and down)

Methods and Rationale
-------------------------

We'll use the GOSeq package to identify categories of genes that are differentially expressed under chronic heat stress. 

GOSeq attempts to correct for size biases when identifying enriched GO categories among differentially expressed genes from RNA-Seq experiments. By size bias, we mean: a tendency for genes with larger transcripts to be found as differentially expressed. Because genes belonging to the same category are often related evolutionarily, they may sometimes have similar transcript sizes, and if their transcripts are very large, this may make these categories more likely to appear in an enrichment analysis. 

Note also that we don't have to use transcript size in the GO analysis; GOSeq can work both with and without transcript size.

To start, we'll assume there is a bias and go from there.

Data Integration
------------

We'll need to do data integration to combine data of multiple types, including:

* Transcript size data
* Differential gene expression data
* GO term definitions
* Tomato GO term annotations

### Transcript sizes

To account for size bias, we need to bring in a source of data about transcript size. For this, we'll use a data file available from the tomato June 2009 QuickLoad site that we've imported into this repository.

```{r}
fname='../ExternalDataSets/tx_sizes.txt.gz'
sizes=read.delim(fname,header=T,sep='\t',as.is=T)
rnames=sizes$locus
sizes=sizes$bp
names(sizes)=rnames
```

### Differential gene expression data

```{r}
fname='../DifferentialExpression/results/tomatoDE.tsv'
results=read.delim(fname,sep='\t',header=T,as.is=T)
row.names(results)=results$gene
```

### Get GO definitions

This file came originally from the curators at the Sol Genomics network.

```{r}
go_defs=read.delim('../ExternalDataSets/go_defs.tsv.gz',sep='\t',header=T,as.is=T)
```

### Tomato Gene Ontology annotations

```{r}
fname='../ExternalDataSets/ITAG2.4.go.csv.gz'
gene2go=read.delim(fname,sep='\t',header=T,as.is=T)
# annotations are by transcript
# merge gene2go with data frame that maps gene ids
# to transcript ids
fname='../ExternalDataSets/S_lycopersicum_Feb_2014.bed'
bed=read.delim(fname,sep='\t',header=F,as.is=T)[,c(4,13)]
names(bed)=c('txid','locus')
gene2go=merge(gene2go,bed,by.x='GeneID',by.y='txid')[,c('locus','GOID')]
rm(bed)
gene2go=gene2go[!duplicated(gene2go),]
names(gene2go)=c('Gene','category')
```

There were `r length(unique(gene2go$Gene))` genes with at least one GO annotation.

Analysis
--------

Now that we've got the data sets assembled, start the analysis.

Let's consider three sets of genes:

* genes that are differentially expressed (up or down)
* genes that are differentially expressed and up-regulated
* genes that are differentially expressed and down-regulated

For each analysis, we'll need a vector of gene names:

```{r}
up=row.names(results)[which(results$logFC>0)]
down=row.names(results)[which(results$logFC<0)]
de=row.names(results)[which(results$logFC!=0)]
```

Load GOSeq library:

```{r}
# install it if necessary
# load the install script
# source("http://bioconductor.org/biocLite.R") 
# biocLite('goseq')
# load the library
suppressPackageStartupMessages(library(goseq))
```

Define a function to do GO term analysis:

```{r}
do.go=function(v,d,gene2go,go_defs,sizes,fname,FDR=0.05){
  background=gene2go$Gene
  background=background[!duplicated(background)]
  names(background)=background
  indexes=which(names(background)%in%v)
  gene.vector=rep(0,length(background))
  gene.vector[indexes]=1
  names(gene.vector)=names(background)
  pwf<-nullp(gene.vector,bias.data=sizes[names(gene.vector)])
  GO=goseq(pwf,gene2cat=gene2go,method="Wallenius")
  GO=merge(GO,go_defs,by.x="category",by.y="id",all.x=T)
  GO[,2]=p.adjust(GO[,2],method='BH')
  GO[,3]=p.adjust(GO[,3],method='BH')
  names(GO)[2]='over.fdr'
  names(GO)[3]='under.fdr'
  toreport=union(which(GO$over.fdr<=FDR),which(GO$under.fdr<=FDR))
  GO=GO[toreport,]
  o=order(GO$over.fdr,decreasing=F)
  GO=GO[o,]
  write.table(GO,file=fname,row.names=F,quote=T,sep="\t")
  GO
}
FDR=0.05
```

### GO analysis of differential expressed genes (both up and down)

```{r fig.width=6,fig.height=6,message=FALSE,warning=FALSE}
fname="results/GO_all_de.tsv"
GO.all=do.go(de,d,gene2go,go_defs,sizes,fname,FDR)
GO=GO.all
```


This analysis created a tab-separated file named `r fname` and identified `r length(which(GO$over.fdr<=FDR))` enriched GO categories and `r length(which(GO$under.fdr<=FDR))` depleted GO categories using DR `r FDR`.

### GO analysis of down-regulated genes only

In the following code, we'll look for GO category enrichment or depletion among the down-regulated genes only.

```{r fig.width=6,fig.height=6,message=FALSE,warning=FALSE}
fname="results/GO_down.tsv"
GO.down=do.go(down,d,gene2go,go_defs,sizes,fname)
GO=GO.down
```


This analysis created a tab-separated file named `r fname` and identified `r length(which(GO$over.fdr<=FDR))` enriched GO categories and `r length(which(GO$under.fdr<=FDR))` depleted GO categories using DR `r FDR`.


### GO analysis of up-regulated genes only

In the following code, we'll look for GO category enrichment or depletion among the up-regulated genes only.

```{r fig.width=6,fig.height=6,message=FALSE,warning=FALSE}
fname="results/GO_up.tsv"
GO.up=do.go(up,d,gene2go,go_defs,sizes,fname)
GO=GO.up
```

This analysis created a tab-separated file named `r fname` and identified `r length(which(GO$over.fdr<=FDR))` enriched GO categories and `r length(which(GO$under.fdr<=FDR))` depleted GO categories using DR `r FDR`.

Summary
----------

There were some GO categories that were enriched with differentially expressed genes. Categories with differential expression included:

```{r}
GO.all[,c('category','numDEInCat','numInCat','name')]
```

Categories enriched with DE, down-regulated genes were:

```{r}
GO.down[,c('category','numDEInCat','numInCat','name')]
```

Categories enriched with DE, up-regulated genes were:

```{r}
GO.up[,c('category','numDEInCat','numInCat','name')]
```


Writing files
-------------

To interpret these results, we need to know which genes in the enriched categories are up- or down-regulated. Let's write out a list of these genes that belong to the significant categories along with their descriptions to facilitate further analysis.

```{r}
GO=rbind(GO.all,GO.down,GO.up)
GO=GO[GO$over.fdr<=FDR,]
GO=GO[!duplicated(GO$category),
      c('category','name','defn')]
de.categories=merge(results[,c('gene','logFC','Cn','Tr','description')],
                    gene2go,by.x='gene',
                    by.y='Gene')
sig.gos=merge(de.categories,GO,by.x='category',
              by.y='category')
o=order(sig.gos$category)
sig.gos=sig.gos[o,c('category','name','gene','logFC','Cn',
                    'Tr','description')]
fname='results/genesInSigGO.tsv'
write.table(sig.gos,file=fname,row.names=F,sep='\t',quote=F)
```

Conclusions
-----------

Genes related to calcium ion binding, protein synthesis, microtubule-based movement, and transport across a membrane were more frequently DE than would be expected by chance, suggesting that chronic, long-term heat stress modifies these processes by change the epxression levels of some but not all genes in these categories. 


Session information
-------------------

```{r}
sessionInfo()
```